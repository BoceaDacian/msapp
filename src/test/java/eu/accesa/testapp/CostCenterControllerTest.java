package eu.accesa.testapp;

import eu.accesa.testapp.controller.CostCenterController;
import eu.accesa.testapp.controller.ServiceCostCenterRelationController;
import eu.accesa.testapp.model.CostCenterModel;
import eu.accesa.testapp.model.ServiceCostCenterRelation;
import eu.accesa.testapp.service.CostCenterService;
import eu.accesa.testapp.service.ServiceCostCenterRelationService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Arrays;

import static org.mockito.Matchers.*;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class CostCenterControllerTest {
    @Mock
    private CostCenterService costCenterService;
    @Mock
    private ServiceCostCenterRelationService serviceCostCenterRelationService;

    @InjectMocks
    private CostCenterController costCenterController;
    @InjectMocks
    private ServiceCostCenterRelationController serviceCostCenterRel;

    private MockMvc mockMvc;
    private MockMvc mockMvc1;

    @Before
    public void setUp(){
        MockitoAnnotations.initMocks(this);
        mockMvc= MockMvcBuilders.standaloneSetup(costCenterController).build();
        mockMvc1= MockMvcBuilders.standaloneSetup(serviceCostCenterRel).build();

        CostCenterModel c1=new CostCenterModel();
        c1.setId(1L);c1.setName("test1");
        CostCenterModel c2=new CostCenterModel();
        c2.setId(2L);c2.setName("test2");
        when(costCenterService.findAll()).thenReturn(Arrays.asList(c1,c2));

        ServiceCostCenterRelation serv=new ServiceCostCenterRelation();
        serv.setCost(1);serv.setId(1L);serv.setRef2US(null);serv.setRef2Dienst(null);serv.setPeriod(1);
        when(serviceCostCenterRelationService.save(anyString(),anyLong(),anyInt())).thenReturn(serv);
    }
    @Test
    public void testGetCostCenters() throws Exception{
        mockMvc.perform(get("/support")).andExpect(status().isOk()).andDo(print());
    }
    @Test
    public void testPostServiceCostCenterRelation() throws Exception{
        mockMvc1.perform(post("/costs").contentType(MediaType.APPLICATION_JSON).content("{\"cost\":1 , \"appId\":\"1L\" ,\"supportId\":1}")).andExpect(status().isOk()).andDo(print());
    }
}
