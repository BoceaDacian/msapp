angular.module('demo').controller('ServiceCostCenterRelationController', function($scope,$http,NgTableParams) {
        $scope.addAppSupport = function () {
            var data = {'appId': $scope.serviceApplicationId, 'supportId': $scope.costCenterId, 'cost': this.val};
                if(this.val==undefined) return;
                $http({
                    method: 'POST',
                    url: '/costs',
                    data: data
                }).then(function (response) {
                    response.data.Dienstname=response.data.ref2Dienst.name
                    response.data.ref2Dienst = response.data.ref2Dienst.id;
                    response.data.ref2US = response.data.ref2US.id;
                    $scope.serviceCostCenterRelation.push(response.data);
                    $scope.serviceCostCenterRelationTableParams=new NgTableParams({noPager : true, count:$scope.serviceCostCenterRelation.length, sorting : {name: "asc" }}, { counts:[], dataset: $scope.serviceCostCenterRelation });
                }, function (error) {
                    console.log(error);
                });
            $scope.toggleModal();
        },
        $scope.modalShown = false;
        $scope.toggleModal = function () {
                $scope.modalShown = !$scope.modalShown;
        };
        $scope.handleDrop = function () {
                $scope.toggleModal();
        }
        $scope.setSelected = function () {
                if ($scope.lastSelected) {
                    $scope.lastSelected.selected = '';
                }
                this.selected = 'selected';
                $scope.lastSelected = this;
        },
        $scope.$on('getCostCenterRelationData',function (event, idSelectedItem) {
            $scope.costCenterId = idSelectedItem;
            $http({
                method: 'GET',
                url: '/supportById',
                params: {id: idSelectedItem}
            }).success(function (data) {
                for (var i = 0; i < data.length; ++i) {
                        data[i].ref2US = data[i].ref2US.id;
                        data[i].Dienstname = data[i].ref2Dienst.name;
                        data[i].ref2Dienst = data[i].ref2Dienst.id;
                }
                $scope.serviceCostCenterRelation = data;
                $scope.serviceCostCenterRelationTableParams=new NgTableParams({noPager : true, count:$scope.serviceCostCenterRelation.length, sorting : {name: "asc" }}, { counts:[], dataset: $scope.serviceCostCenterRelation });
            });
        });
})
