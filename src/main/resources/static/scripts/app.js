angular.module('demo', ["ngTable"])
    .controller('myCtrl', function($scope, $http, NgTableParams) {
        $scope.id = 0;
        $scope.val = 0;

        $scope.costCenterId = null;
        $scope.serviceApplicationId = null;
        $scope.serviceCostCenterRelation = [];
        $scope.lastSelected = [null, null, null];
        $scope.searchInput=null;
        $scope.serviceApplicationBackup=[];
        $scope.serviceApplicationColumn=[];

        $scope.search=function(){
            $scope.serviceApplication=$scope.serviceApplicationBackup;
            $scope.serviceApplicationTemp=[];
            if($scope.searchInput==""){
                $scope.serviceApplication=$scope.serviceApplicationBackup;
                  $scope.serviceApplicationTableParams = new NgTableParams({noPager : true, count:$scope.serviceApplication.length, sorting : {name: "asc" }}, { counts:[], dataset: $scope.serviceApplication });
                return;
            }
            for(var i=0;i<$scope.serviceApplication.length;++i){
                if($scope.serviceApplication[i].id.includes($scope.searchInput)){
                    $scope.serviceApplicationTemp.push($scope.serviceApplication[i]);
                }
            }
            $scope.serviceApplication=$scope.serviceApplicationTemp;
            $scope.serviceApplicationTableParams = new NgTableParams({noPager : true, count:$scope.serviceApplication.length, sorting : {name: "asc" }}, { counts:[], dataset: $scope.serviceApplication });
        }
    })
    .directive('modalDialog', function() {
        return {
          restrict: 'E',
          scope: {
            show: '='
          },
          transclude: true,
          link: function(scope, element, attrs) {
            scope.dialogStyle = {};
            if (attrs.width)
              scope.dialogStyle.width = attrs.width;
            if (attrs.height)
              scope.dialogStyle.height = attrs.height;
            scope.hideModal = function() {
              scope.show = false;
            };
          },
          templateUrl: 'templates/modal.html'
        };}
    )
    .directive('draggable', function() {
         return {
             scope:false,
             link : function($scope, element) {
             var el = element[0];
             el.draggable = true;
             el.addEventListener(
                 'dragstart',
                 function(e) {
                       e.dataTransfer.effectAllowed = 'move';
                       e.dataTransfer.setData('Text', this.id);
                       this.classList.add('drag');
                       console.log(this.id);
                       return false;
                 },
                 false
             );
             el.addEventListener(
                  'dragend',
                  function() {
                        this.classList.remove('drag');
                        return false;
                  },
                  false
             );
         }}
      }
     )
     .directive('droppable', function() {
         return {
             scope: {
                 drop: '&'
             },
             link: function (scope, element) {
                 var el = element[0];
                 el.addEventListener(
                     'dragover',
                     function (e) {
                         e.dataTransfer.dropEffect = 'move';
                         if (e.preventDefault)
                             e.preventDefault();
                         this.classList.add('over');
                         return false;
                     },
                     false
                 );
                 el.addEventListener(
                     'drop',
                     function (e) {
                         if (e.stopPropagation)
                             e.stopPropagation();
                         this.classList.remove('over');
                         scope.$apply('drop()');
                         return false;
                     },
                     false
                 );
             }
         }
     })
