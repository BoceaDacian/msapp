angular.module('demo').controller('UtilityController', function($scope,$rootScope) {
        var date=new Date();
        $scope.inputDate = date.getFullYear()*100+date.getMonth()+1;
        $scope.incrementButton = function () {
            var day=$scope.inputDate % 100;
            if(day>=12 || day ==0)
                $scope.inputDate=$scope.inputDate+100-$scope.inputDate%100+1;
            else $scope.inputDate += 1;
            $rootScope.$broadcast('getByDate',$scope.inputDate);
        },
        $scope.$watch('inputDate', function () {
                $rootScope.$broadcast('getByDate',$scope.inputDate);
        }),
        $scope.decrementButton = function () {
            var day=$scope.inputDate % 100;
                if(day>12 || day ==1)
                    $scope.inputDate=$scope.inputDate-100-$scope.inputDate%100+12;
                else $scope.inputDate-=1;
            $rootScope.$broadcast('getByDate',$scope.inputDate);
        }
})
