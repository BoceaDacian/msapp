angular.module('demo').controller('ServiceApplicationController', function($rootScope,$scope,$http,NgTableParams) {
    $scope.$on('getByDate',function (event ,inputDate) {
            $scope.getByDate(inputDate);
    }),
    $scope.getAllserviceApplications = function () {
            $http({
                  method: 'GET',
                  url: '/services'
            }).then(function successCallback(response) {
                  $scope.serviceApplication = response.data;
                  $scope.serviceApplicationBackup=response.data;
                  $scope.serviceApplicationTableParams = new NgTableParams({noPager : true, count:$scope.serviceApplication.length, sorting : {name: "asc" }}, { counts:[], dataset: $scope.serviceApplication });
            });
    },
    $scope.setSelected = function () {
            if ($scope.lastSelected) $scope.lastSelected.selected = '';
            this.selected = 'selected';
            $scope.lastSelected = this;
            $rootScope.serviceApplicationId=this.serviceApp.id;
    },
    $scope.getByDate=function(inputDate){
            if (inputDate != null) {
                  $http.get("/services/" + inputDate)
                  .then(function (response) {
                       $scope.serviceApplication = response.data;
                       $scope.data=$scope.serviceApplication
                       $scope.serviceApplicationBackup=response.data;
                       $scope.serviceApplicationTableParams = new NgTableParams({noPager : true, count:$scope.serviceApplication.length, sorting : {name: "asc" }}, { counts:[], dataset: $scope.serviceApplication });
                  });
            }
            else $scope.getAllserviceApplications();
    }
    $scope.initAppServiceTable=function(){
            var date=new Date();
            $scope.inputDate = date.getFullYear()*100+date.getMonth()+1;
            $scope.getByDate($scope.inputDate);
    }
    $scope.initAppServiceTable();
})
