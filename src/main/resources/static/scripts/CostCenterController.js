angular.module('demo').controller('CostCenterController', function($rootScope,$scope,$http,NgTableParams) {
        $http({
              method: 'GET',
              url: '/support'
        }).then(function successCallback(response) {
              $scope.costCenter = response.data;
              $scope.costCenterTableParams = new NgTableParams({noPager : true, count:$scope.costCenter.length, sorting : {name: "asc" }}, { counts:[], dataset: $scope.costCenter });
        }),
        function errorCallback() {
              $scope.costCenter = [];
        },
        $scope.setSelected = function () {
              if ($scope.lastSelected)
                         $scope.lastSelected.selected = '';
              this.selected = 'selected';
              $scope.lastSelected = this;
              $rootScope.$broadcast('getCostCenterRelationData', this.costCenter.id)
        }
 })

