package eu.accesa.testapp.repository;

import eu.accesa.testapp.model.CostCenterModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CostCenterRepo extends JpaRepository<CostCenterModel, Long> {
}