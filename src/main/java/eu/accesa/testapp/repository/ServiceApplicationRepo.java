package eu.accesa.testapp.repository;

import eu.accesa.testapp.model.ServiceApplicationModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ServiceApplicationRepo extends JpaRepository<ServiceApplicationModel,String>{

    List<ServiceApplicationModel> findByPer(int per);
}
