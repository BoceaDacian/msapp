package eu.accesa.testapp.repository;

import eu.accesa.testapp.model.BusinessServices;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BusinessServicesRepo extends JpaRepository<BusinessServices, Integer> {
}
