package eu.accesa.testapp.repository;

import eu.accesa.testapp.model.BusinessSystems;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BusinessSystemsRepo extends JpaRepository<BusinessSystems, Integer> {
}
