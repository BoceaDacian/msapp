package eu.accesa.testapp.repository;


import eu.accesa.testapp.model.ServiceCostCenterRelation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ServiceCostCenterRelationRepo extends JpaRepository<ServiceCostCenterRelation, Long> {
}
