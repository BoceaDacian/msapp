package eu.accesa.testapp.repository;

import eu.accesa.testapp.model.LogEntries;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LogEntriesRepo extends JpaRepository<LogEntries, Integer>{
}
