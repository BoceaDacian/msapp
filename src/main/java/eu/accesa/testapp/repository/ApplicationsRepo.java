package eu.accesa.testapp.repository;

import eu.accesa.testapp.model.Applications;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ApplicationsRepo extends JpaRepository<Applications, Integer> {
}
