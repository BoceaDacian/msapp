package eu.accesa.testapp.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.sql.Time;
import java.util.Date;

/**
 * Created by dragos.doicar on 11/1/2016.
 */
@Entity(name="ADMIN.SCM_LOG_ENTRIES")
public class LogEntries {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    @NotNull
    @Column(name="table_name")
    private String tableName;
    @NotNull
    @Column(name="row_id")
    private int rowId;
    @NotNull
    @Column(name="column_name")
    private String columnName;
    @Column(name="old_value")
    private String oldValue;
    @Column(name="old_text")
    private String oldText;
    @Column(name="new_value")
    private String newValue;
    @Column(name="new_text")
    private String newText;
    private String caller;
    @Column(name="logged_by_users_id")
    private int loggedByUsersId;
    @Column(name="logged_date")
    private Date loggedDate;
    @Column(name="logged_time")
    private Time loggedTime;

    public LogEntries(String tableName, int rowId, String columnName, String oldValue, String oldText, String newValue, String newText, String caller, int loggedByUsersId, Date loggedDate, Time loggedTime) {
        this.tableName = tableName;
        this.rowId = rowId;
        this.columnName = columnName;
        this.oldValue = oldValue;
        this.oldText = oldText;
        this.newValue = newValue;
        this.newText = newText;
        this.caller = caller;
        this.loggedByUsersId = loggedByUsersId;
        this.loggedDate = loggedDate;
        this.loggedTime = loggedTime;
    }

    public LogEntries() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public int getRowId() {
        return rowId;
    }

    public void setRowId(int rowId) {
        this.rowId = rowId;
    }

    public String getColumnName() {
        return columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    public String getOldValue() {
        return oldValue;
    }

    public void setOldValue(String oldValue) {
        this.oldValue = oldValue;
    }

    public String getOldText() {
        return oldText;
    }

    public void setOldText(String oldText) {
        this.oldText = oldText;
    }

    public String getNewValue() {
        return newValue;
    }

    public void setNewValue(String newValue) {
        this.newValue = newValue;
    }

    public String getNewText() {
        return newText;
    }

    public void setNewText(String newText) {
        this.newText = newText;
    }

    public String getCaller() {
        return caller;
    }

    public void setCaller(String caller) {
        this.caller = caller;
    }

    public int getLoggedByUsersId() {
        return loggedByUsersId;
    }

    public void setLoggedByUsersId(int loggedByUsersId) {
        this.loggedByUsersId = loggedByUsersId;
    }

    public Date getLoggedDate() {
        return loggedDate;
    }

    public void setLoggedDate(Date loggedDate) {
        this.loggedDate = loggedDate;
    }

    public Time getLoggedTime() {
        return loggedTime;
    }

    public void setLoggedTime(Time loggedTime) {
        this.loggedTime = loggedTime;
    }
}
