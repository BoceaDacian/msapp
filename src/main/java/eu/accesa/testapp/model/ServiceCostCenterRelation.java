package eu.accesa.testapp.model;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(uniqueConstraints={
        @UniqueConstraint(columnNames = {"ref2us_id", "ref2dienst_id"})
})
public class ServiceCostCenterRelation implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private int period;
    private int cost;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn
    private ServiceApplicationModel ref2Dienst;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn
    private CostCenterModel ref2US;

    public ServiceCostCenterRelation(Long id, int period, int cost, ServiceApplicationModel serviceApplicationModelId, CostCenterModel costCenterModelId) {
        this.period = period;
        this.cost = cost;
        this.ref2Dienst = serviceApplicationModelId;
        this.ref2US = costCenterModelId;
        this.id=id;
    }

    public ServiceCostCenterRelation() {
        //default constructor
    }

    public int getPeriod() {
        return period;
    }

    public void setPeriod(int period) {
        this.period = period;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @JsonIgnoreProperties({"status","aoDepartment","per"})
    public ServiceApplicationModel getRef2Dienst() {
        return ref2Dienst;
    }

    public void setRef2Dienst(ServiceApplicationModel service) {
        ref2Dienst = service;
    }

    @JsonIgnoreProperties({"name","appSupportList"})
    public CostCenterModel getRef2US() {
        return ref2US;
    }

    public void setRef2US(CostCenterModel ref2US) {
        this.ref2US = ref2US;
    }
}
