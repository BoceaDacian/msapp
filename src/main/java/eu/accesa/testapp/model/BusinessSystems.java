package eu.accesa.testapp.model;


import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.xml.stream.StreamFilter;
import java.sql.Time;
import java.util.Date;

/**
 * Created by dragos.doicar on 11/1/2016.
 */
@Entity(name="ADMIN.SCM_BUSINESS_SYSTEMS")
public class BusinessSystems {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    @NotNull
    private String name;
    private String description;
    private String remark;
    @NotNull
    @Column(name="business_service_id")
    private int businessServiceId;
    @Column(name="sol_id")
    private int solId;
    @Column(name="created_by_user_id")
    private int createdByUsersId;
    @Column(name="created_date")
    private Date createdDate;
    @Column(name="created_time")
    private Time createdTime;
    @Column(name="modified_by_user_id")
    private int modifiedByUsersId;
    @Column(name="modified_date")
    private Date modifiedDate;
    @Column(name="modified_time")
    private Time modifiedTime;
    @NotNull
    @Column(name="is_active")
    private int isActive;
    @Column(name="system_id")
    private String systemId;
    @NotNull
    private String status;

    public BusinessSystems(String name, String description, String remark, int businessServiceId, int solId, int createdByUsersId, Date createdDate, Time createdTime, int modifiedByUsersId, Date modifiedDate, Time modifiedTime, int isActive, String systemId, String status) {
        this.name = name;
        this.description = description;
        this.remark = remark;
        this.businessServiceId = businessServiceId;
        this.solId = solId;
        this.createdByUsersId = createdByUsersId;
        this.createdDate = createdDate;
        this.createdTime = createdTime;
        this.modifiedByUsersId = modifiedByUsersId;
        this.modifiedDate = modifiedDate;
        this.modifiedTime = modifiedTime;
        this.isActive = isActive;
        this.systemId = systemId;
        this.status = status;
    }

    public BusinessSystems() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public int getBusinessServiceId() {
        return businessServiceId;
    }

    public void setBusinessServiceId(int businessServiceId) {
        this.businessServiceId = businessServiceId;
    }

    public int getSolId() {
        return solId;
    }

    public void setSolId(int solId) {
        this.solId = solId;
    }

    public int getCreatedByUsersId() {
        return createdByUsersId;
    }

    public void setCreatedByUsersId(int createdByUsersId) {
        this.createdByUsersId = createdByUsersId;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Time getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Time createdTime) {
        this.createdTime = createdTime;
    }

    public int getModifiedByUsersId() {
        return modifiedByUsersId;
    }

    public void setModifiedByUsersId(int modifiedByUsersId) {
        this.modifiedByUsersId = modifiedByUsersId;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public Time getModifiedTime() {
        return modifiedTime;
    }

    public void setModifiedTime(Time modifiedTime) {
        this.modifiedTime = modifiedTime;
    }

    public int getIsActive() {
        return isActive;
    }

    public void setIsActive(int isActive) {
        this.isActive = isActive;
    }

    public String getSystemId() {
        return systemId;
    }

    public void setSystemId(String systemId) {
        this.systemId = systemId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
