package eu.accesa.testapp.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.io.Serializable;
import java.util.List;

@Entity
public class ServiceApplicationModel implements Serializable {
    private int per;
    @Id
    private String id;
    private String name;
    private String status;
    private String aoDepartment;
    @OneToMany(fetch = FetchType.EAGER,mappedBy = "ref2Dienst")
    @JsonIgnore
    private List<ServiceCostCenterRelation> serviceCostCenterRelationList;

    public ServiceApplicationModel() {
        //default constructor
    }


    public ServiceApplicationModel(int per, String id, String name, String status, String aoDepartment) {
        this.per = per;
        this.id = id;
        this.name = name;
        this.status = status;
        this.aoDepartment = aoDepartment;
    }

    public int getPer() {
        return per;
    }

    public void setPer(int per) {
        this.per = per;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAoDepartment() {
        return aoDepartment;
    }

    public void setAoDepartment(String aoDepartment) {
        this.aoDepartment = aoDepartment;
    }

    public List<ServiceCostCenterRelation> getServiceCostCenterRelationList() {
        return serviceCostCenterRelationList;
    }

    public void setServiceCostCenterRelationList(List<ServiceCostCenterRelation> serviceCostCenterRelationList) {
        this.serviceCostCenterRelationList = serviceCostCenterRelationList;
    }
}
