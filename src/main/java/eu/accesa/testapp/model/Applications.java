package eu.accesa.testapp.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.sql.Time;
import java.util.Date;

/**
 * Created by dragos.doicar on 11/1/2016.
 */
@Entity(name="ADMIN.SCM_APPLICATIONS")
public class Applications {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    @NotNull
    @Column(name="planningIt_type")
    private String planningItType;
    @NotNull
    @Column(name="planningIt_id")
    private String planningItId;
    @NotNull
    private String name;
    private String description;
    private String remark;
    @Column(name="start_date")
    private Date startDate;
    @Column(name="end_date")
    private Date endDate;
    private String domain;
    private String version;
    private String status;
    private String criticality;
    @Column(name="bso_unit")
    private String bsoUnit;
    @Column(name="bso_customer")
    private String bsoCustomer;
    @Column(name="bso_application")
    private String bsoApplication;
    @Column(name="application_owner")
    private String applicationOwner;
    @Column(name="ao_department")
    private String aoDepartment;
    @Column(name="solution_owner")
    private String solutionOwner;
    @Column(name="so_department")
    private String soDepartment;
    @Column(name="owning_company")
    private String owningCompany;
    @Column(name="application_vendor")
    private String applicationVendor;
    @Column(name="sol_id")
    private int solId;
    @Column(name="created_by_user_id")
    private int createdByUserId;
    @Column(name="created_date")
    private Date createdDate;
    @Column(name="created_time")
    private Time createdTime;
    @Column(name="modified_by_users_id")
    private int modifiedByUsersId;
    @Column(name="modified_date")
    private Date modifiedDate;
    @Column(name="modified_time")
    private Time modifiedTime;
    @NotNull
    @Column(name="is_active")
    private int isActive;
    @Column(name="status_pit")
    private String statusPit;
    @Column(name="criticality_pit")
    private String criticalityPit;
    @Column(name="application_owner_bp_id")
    private int applicationOwnerBpId;
    @Column(name="application_owner_pit")
    private String applicationOwnerPit;
    @Column(name="ao_departament_pit")
    private String aoDepartmentPit;
    @Column(name="modified_date_pit")
    private Date modifiedDatePit;
    @Column(name="modified_time_pit")
    private Time modifiedTimePit;
    @Column(name="last_checked_by_users_id")
    private int lastCheckedByUsersId;
    @Column(name="last_checked_date")
    private Date lastCheckedDate;
    @Column(name="last_checked_time")
    private Time lastCheckedTime;
    @Column(name="modified_date_venus")
    private Date modifiedDateVenus;
    @Column(name="modifed_time_venus")
    private Time modifedTimeVenus;

    public Applications(String planningItType, String planningItId, String name, String description, String remark, Date startDate, Date endDate, String domain, String version, String status, String criticality, String bsoUnit, String bsoCustomer, String bsoApplication, String applicationOwner, String aoDepartment, String solutionOwner, String soDepartment, String owningCompany, String applicationVendor, int solId, int createdByUserId, Date createdDate, Time createdTime, int modifiedByUsersId, Date modifiedDate, Time modifiedTime, int isActive, String statusPit, String criticalityPit, int applicationOwnerBpId, String applicationOwnerPit, String aoDepartmentPit, Date modifiedDatePit, Time modifiedTimePit, int lastCheckedByUsersId, Date lastCheckedDate, Time lastCheckedTime, Date modifiedDateVenus, Time modifedTimeVenus) {
        this.planningItType = planningItType;
        this.planningItId = planningItId;
        this.name = name;
        this.description = description;
        this.remark = remark;
        this.startDate = startDate;
        this.endDate = endDate;
        this.domain = domain;
        this.version = version;
        this.status = status;
        this.criticality = criticality;
        this.bsoUnit = bsoUnit;
        this.bsoCustomer = bsoCustomer;
        this.bsoApplication = bsoApplication;
        this.applicationOwner = applicationOwner;
        this.aoDepartment = aoDepartment;
        this.solutionOwner = solutionOwner;
        this.soDepartment = soDepartment;
        this.owningCompany = owningCompany;
        this.applicationVendor = applicationVendor;
        this.solId = solId;
        this.createdByUserId = createdByUserId;
        this.createdDate = createdDate;
        this.createdTime = createdTime;
        this.modifiedByUsersId = modifiedByUsersId;
        this.modifiedDate = modifiedDate;
        this.modifiedTime = modifiedTime;
        this.isActive = isActive;
        this.statusPit = statusPit;
        this.criticalityPit = criticalityPit;
        this.applicationOwnerBpId = applicationOwnerBpId;
        this.applicationOwnerPit = applicationOwnerPit;
        this.aoDepartmentPit = aoDepartmentPit;
        this.modifiedDatePit = modifiedDatePit;
        this.modifiedTimePit = modifiedTimePit;
        this.lastCheckedByUsersId = lastCheckedByUsersId;
        this.lastCheckedDate = lastCheckedDate;
        this.lastCheckedTime = lastCheckedTime;
        this.modifiedDateVenus = modifiedDateVenus;
        this.modifedTimeVenus = modifedTimeVenus;
    }

    public Applications() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPlanningItType() {
        return planningItType;
    }

    public void setPlanningItType(String planningItType) {
        this.planningItType = planningItType;
    }

    public String getPlanningItId() {
        return planningItId;
    }

    public void setPlanningItId(String planningItId) {
        this.planningItId = planningItId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCriticality() {
        return criticality;
    }

    public void setCriticality(String criticality) {
        this.criticality = criticality;
    }

    public String getBsoUnit() {
        return bsoUnit;
    }

    public void setBsoUnit(String bsoUnit) {
        this.bsoUnit = bsoUnit;
    }

    public String getBsoCustomer() {
        return bsoCustomer;
    }

    public void setBsoCustomer(String bsoCustomer) {
        this.bsoCustomer = bsoCustomer;
    }

    public String getBsoApplication() {
        return bsoApplication;
    }

    public void setBsoApplication(String bsoApplication) {
        this.bsoApplication = bsoApplication;
    }

    public String getApplicationOwner() {
        return applicationOwner;
    }

    public void setApplicationOwner(String applicationOwner) {
        this.applicationOwner = applicationOwner;
    }

    public String getAoDepartment() {
        return aoDepartment;
    }

    public void setAoDepartment(String aoDepartment) {
        this.aoDepartment = aoDepartment;
    }

    public String getSolutionOwner() {
        return solutionOwner;
    }

    public void setSolutionOwner(String solutionOwner) {
        this.solutionOwner = solutionOwner;
    }

    public String getSoDepartment() {
        return soDepartment;
    }

    public void setSoDepartment(String soDepartment) {
        this.soDepartment = soDepartment;
    }

    public String getOwningCompany() {
        return owningCompany;
    }

    public void setOwningCompany(String owningCompany) {
        this.owningCompany = owningCompany;
    }

    public String getApplicationVendor() {
        return applicationVendor;
    }

    public void setApplicationVendor(String applicationVendor) {
        this.applicationVendor = applicationVendor;
    }

    public int getSolId() {
        return solId;
    }

    public void setSolId(int solId) {
        this.solId = solId;
    }

    public int getCreatedByUserId() {
        return createdByUserId;
    }

    public void setCreatedByUserId(int createdByUserId) {
        this.createdByUserId = createdByUserId;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Time getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Time createdTime) {
        this.createdTime = createdTime;
    }

    public int getModifiedByUsersId() {
        return modifiedByUsersId;
    }

    public void setModifiedByUsersId(int modifiedByUsersId) {
        this.modifiedByUsersId = modifiedByUsersId;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public Time getModifiedTime() {
        return modifiedTime;
    }

    public void setModifiedTime(Time modifiedTime) {
        this.modifiedTime = modifiedTime;
    }

    public int getIsActive() {
        return isActive;
    }

    public void setIsActive(int isActive) {
        this.isActive = isActive;
    }

    public String getStatusPit() {
        return statusPit;
    }

    public void setStatusPit(String statusPit) {
        this.statusPit = statusPit;
    }

    public String getCriticalityPit() {
        return criticalityPit;
    }

    public void setCriticalityPit(String criticalityPit) {
        this.criticalityPit = criticalityPit;
    }

    public int getApplicationOwnerBpId() {
        return applicationOwnerBpId;
    }

    public void setApplicationOwnerBpId(int applicationOwnerBpId) {
        this.applicationOwnerBpId = applicationOwnerBpId;
    }

    public String getApplicationOwnerPit() {
        return applicationOwnerPit;
    }

    public void setApplicationOwnerPit(String applicationOwnerPit) {
        this.applicationOwnerPit = applicationOwnerPit;
    }

    public String getAoDepartmentPit() {
        return aoDepartmentPit;
    }

    public void setAoDepartmentPit(String aoDepartmentPit) {
        this.aoDepartmentPit = aoDepartmentPit;
    }

    public Date getModifiedDatePit() {
        return modifiedDatePit;
    }

    public void setModifiedDatePit(Date modifiedDatePit) {
        this.modifiedDatePit = modifiedDatePit;
    }

    public Time getModifiedTimePit() {
        return modifiedTimePit;
    }

    public void setModifiedTimePit(Time modifiedTimePit) {
        this.modifiedTimePit = modifiedTimePit;
    }

    public int getLastCheckedByUsersId() {
        return lastCheckedByUsersId;
    }

    public void setLastCheckedByUsersId(int lastCheckedByUsersId) {
        this.lastCheckedByUsersId = lastCheckedByUsersId;
    }

    public Date getLastCheckedDate() {
        return lastCheckedDate;
    }

    public void setLastCheckedDate(Date lastCheckedDate) {
        this.lastCheckedDate = lastCheckedDate;
    }

    public Time getLastCheckedTime() {
        return lastCheckedTime;
    }

    public void setLastCheckedTime(Time lastCheckedTime) {
        this.lastCheckedTime = lastCheckedTime;
    }

    public Date getModifiedDateVenus() {
        return modifiedDateVenus;
    }

    public void setModifiedDateVenus(Date modifiedDateVenus) {
        this.modifiedDateVenus = modifiedDateVenus;
    }

    public Time getModifedTimeVenus() {
        return modifedTimeVenus;
    }

    public void setModifedTimeVenus(Time modifedTimeVenus) {
        this.modifedTimeVenus = modifedTimeVenus;
    }
}
