package eu.accesa.testapp.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.sql.Time;
import java.util.Date;

/**
 * Created by dragos.doicar on 11/1/2016.
 */
@Entity(name="ADMIN.SCM_SERVICE_INSTANCES")
public class ServiceInstances {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    @NotNull
    @Column(name="service_id")
    private String serviceId;
    @NotNull
    private String name;
    @Column(name="application_name")
    private String applicationName;
    @NotNull
    @Column(name="pit_type")
    private String pitType;
    private String status;
    private String criticality;
    private String priority;
    @NotNull
    @Column(name="major_incident")
    private int majorIncident;
    private String availability;
    @Column(name="planned_maintenance")
    private String plannedMaintenance;
    @Column(name="performance_kpi")
    private String performanceKpi;
    private String variation;
    @Column(name="business_service_id")
    private int businessServiceId;
    @Column(name="business_system_id")
    private int businessSystemId;
    @NotNull
    @Column(name="application_id")
    private int applicationId;
    @NotNull
    @Column(name="service_enviroment_id")
    private int serviceEnviromentId;
    @Column(name="service_time_id")
    private int serviceTimeId;
    @Column(name="reliability_id")
    private int reliabilityId;
    @Column(name="reaction_time_name")
    private String reactionTimeName;
    @Column(name="resolution_time_name")
    private String resolutionTimeName;
    @Column(name="level1support_users_id")
    private int level1supportUsersId;
    @Column(name="level2support_users_id")
    private int level2supportUsersId;
    @Column(name="level3support_users_id")
    private int level3supportUsersId;
    @Column(name="responsible_bp_id")
    private int responsibleBpId;
    @Column(name="sol_id")
    private int solId;
    @Column(name="last_checked_by_users_id")
    private int lastCheckedByUsersId;
    @Column(name="last_checked_date")
    private Date lastCheckedDate;
    @Column(name="last_checked_time")
    private Time lastCheckedTime;
    private String remark;
    @Column(name="created_by_users_id")
    private int createdByUsersId;
    @Column(name="created_date")
    private Date createdDate;
    @Column(name="created_time")
    private Time createdTime;
    @Column(name="modified_by_users_id")
    private int modifiedByUsersId;
    @Column(name="modified_date")
    private Date modifiedDate;
    @Column(name="modified_time")
    private Time modifiedTime;
    @NotNull
    @Column(name="is_active")
    private int isActive;
    @NotNull
    @Column(name="major_incident_after_mins")
    private int majorIncidentAfterMins;
    @Column(name="slm_type")
    private String slmType;
    @Column(name="monitoring_category")
    private String monitoringCategory;
    @Column(name="monitoring_application")
    private String monitoringApplication;
    @Column(name="data_confidentiality")
    private String dataConfidentiality;
    @Column(name="level3operation_users_id")
    private int level3operationUsersId;
    @Column(name="infrastructure_contact_bp_id")
    private int infrastructureContactBpId;
    @Column(name="service_type")
    private String serviceType;
    @Column(name="sd2sd_category_id")
    private int sd2sdCategoryId;
    @Column(name="sd2sd_capgemini_service")
    private String sd2sdCapgeminiService;

    public ServiceInstances(String serviceId, String name, String applicationName, String pitType, String status, String criticality, String priority, int majorIncident, String availability, String plannedMaintenance, String performanceKpi, String variation, int businessServiceId, int businessSystemId, int applicationId, int serviceEnviromentId, int serviceTimeId, int reliabilityId, String reactionTimeName, String resolutionTimeName, int level1supportUsersId, int level2supportUsersId, int level3supportUsersId, int responsibleBpId, int solId, int lastCheckedByUsersId, Date lastCheckedDate, Time lastCheckedTime, String remark, int createdByUsersId, Date createdDate, Time createdTime, int modifiedByUsersId, Date modifiedDate, Time modifiedTime, int isActive, int majorIncidentAfterMins, String slmType, String monitoringCategory, String monitoringApplication, String dataConfidentiality, int level3operationUsersId, int infrastructureContactBpId, String serviceType, int sd2sdCategoryId, String sd2sdCapgeminiService) {
        this.serviceId = serviceId;
        this.name = name;
        this.applicationName = applicationName;
        this.pitType = pitType;
        this.status = status;
        this.criticality = criticality;
        this.priority = priority;
        this.majorIncident = majorIncident;
        this.availability = availability;
        this.plannedMaintenance = plannedMaintenance;
        this.performanceKpi = performanceKpi;
        this.variation = variation;
        this.businessServiceId = businessServiceId;
        this.businessSystemId = businessSystemId;
        this.applicationId = applicationId;
        this.serviceEnviromentId = serviceEnviromentId;
        this.serviceTimeId = serviceTimeId;
        this.reliabilityId = reliabilityId;
        this.reactionTimeName = reactionTimeName;
        this.resolutionTimeName = resolutionTimeName;
        this.level1supportUsersId = level1supportUsersId;
        this.level2supportUsersId = level2supportUsersId;
        this.level3supportUsersId = level3supportUsersId;
        this.responsibleBpId = responsibleBpId;
        this.solId = solId;
        this.lastCheckedByUsersId = lastCheckedByUsersId;
        this.lastCheckedDate = lastCheckedDate;
        this.lastCheckedTime = lastCheckedTime;
        this.remark = remark;
        this.createdByUsersId = createdByUsersId;
        this.createdDate = createdDate;
        this.createdTime = createdTime;
        this.modifiedByUsersId = modifiedByUsersId;
        this.modifiedDate = modifiedDate;
        this.modifiedTime = modifiedTime;
        this.isActive = isActive;
        this.majorIncidentAfterMins = majorIncidentAfterMins;
        this.slmType = slmType;
        this.monitoringCategory = monitoringCategory;
        this.monitoringApplication = monitoringApplication;
        this.dataConfidentiality = dataConfidentiality;
        this.level3operationUsersId = level3operationUsersId;
        this.infrastructureContactBpId = infrastructureContactBpId;
        this.serviceType = serviceType;
        this.sd2sdCategoryId = sd2sdCategoryId;
        this.sd2sdCapgeminiService = sd2sdCapgeminiService;
    }

    public ServiceInstances() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getApplicationName() {
        return applicationName;
    }

    public void setApplicationName(String applicationName) {
        this.applicationName = applicationName;
    }

    public String getPitType() {
        return pitType;
    }

    public void setPitType(String pitType) {
        this.pitType = pitType;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCriticality() {
        return criticality;
    }

    public void setCriticality(String criticality) {
        this.criticality = criticality;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public int getMajorIncident() {
        return majorIncident;
    }

    public void setMajorIncident(int majorIncident) {
        this.majorIncident = majorIncident;
    }

    public String getAvailability() {
        return availability;
    }

    public void setAvailability(String availability) {
        this.availability = availability;
    }

    public String getPlannedMaintenance() {
        return plannedMaintenance;
    }

    public void setPlannedMaintenance(String plannedMaintenance) {
        this.plannedMaintenance = plannedMaintenance;
    }

    public String getPerformanceKpi() {
        return performanceKpi;
    }

    public void setPerformanceKpi(String performanceKpi) {
        this.performanceKpi = performanceKpi;
    }

    public String getVariation() {
        return variation;
    }

    public void setVariation(String variation) {
        this.variation = variation;
    }

    public int getBusinessServiceId() {
        return businessServiceId;
    }

    public void setBusinessServiceId(int businessServiceId) {
        this.businessServiceId = businessServiceId;
    }

    public int getBusinessSystemId() {
        return businessSystemId;
    }

    public void setBusinessSystemId(int businessSystemId) {
        this.businessSystemId = businessSystemId;
    }

    public int getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(int applicationId) {
        this.applicationId = applicationId;
    }

    public int getServiceEnviromentId() {
        return serviceEnviromentId;
    }

    public void setServiceEnviromentId(int serviceEnviromentId) {
        this.serviceEnviromentId = serviceEnviromentId;
    }

    public int getServiceTimeId() {
        return serviceTimeId;
    }

    public void setServiceTimeId(int serviceTimeId) {
        this.serviceTimeId = serviceTimeId;
    }

    public int getReliabilityId() {
        return reliabilityId;
    }

    public void setReliabilityId(int reliabilityId) {
        this.reliabilityId = reliabilityId;
    }

    public String getReactionTimeName() {
        return reactionTimeName;
    }

    public void setReactionTimeName(String reactionTimeName) {
        this.reactionTimeName = reactionTimeName;
    }

    public String getResolutionTimeName() {
        return resolutionTimeName;
    }

    public void setResolutionTimeName(String resolutionTimeName) {
        this.resolutionTimeName = resolutionTimeName;
    }

    public int getLevel1supportUsersId() {
        return level1supportUsersId;
    }

    public void setLevel1supportUsersId(int level1supportUsersId) {
        this.level1supportUsersId = level1supportUsersId;
    }

    public int getLevel2supportUsersId() {
        return level2supportUsersId;
    }

    public void setLevel2supportUsersId(int level2supportUsersId) {
        this.level2supportUsersId = level2supportUsersId;
    }

    public int getLevel3supportUsersId() {
        return level3supportUsersId;
    }

    public void setLevel3supportUsersId(int level3supportUsersId) {
        this.level3supportUsersId = level3supportUsersId;
    }

    public int getResponsibleBpId() {
        return responsibleBpId;
    }

    public void setResponsibleBpId(int responsibleBpId) {
        this.responsibleBpId = responsibleBpId;
    }

    public int getSolId() {
        return solId;
    }

    public void setSolId(int solId) {
        this.solId = solId;
    }

    public int getLastCheckedByUsersId() {
        return lastCheckedByUsersId;
    }

    public void setLastCheckedByUsersId(int lastCheckedByUsersId) {
        this.lastCheckedByUsersId = lastCheckedByUsersId;
    }

    public Date getLastCheckedDate() {
        return lastCheckedDate;
    }

    public void setLastCheckedDate(Date lastCheckedDate) {
        this.lastCheckedDate = lastCheckedDate;
    }

    public Time getLastCheckedTime() {
        return lastCheckedTime;
    }

    public void setLastCheckedTime(Time lastCheckedTime) {
        this.lastCheckedTime = lastCheckedTime;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public int getCreatedByUsersId() {
        return createdByUsersId;
    }

    public void setCreatedByUsersId(int createdByUsersId) {
        this.createdByUsersId = createdByUsersId;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Time getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Time createdTime) {
        this.createdTime = createdTime;
    }

    public int getModifiedByUsersId() {
        return modifiedByUsersId;
    }

    public void setModifiedByUsersId(int modifiedByUsersId) {
        this.modifiedByUsersId = modifiedByUsersId;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public Time getModifiedTime() {
        return modifiedTime;
    }

    public void setModifiedTime(Time modifiedTime) {
        this.modifiedTime = modifiedTime;
    }

    public int getIsActive() {
        return isActive;
    }

    public void setIsActive(int isActive) {
        this.isActive = isActive;
    }

    public int getMajorIncidentAfterMins() {
        return majorIncidentAfterMins;
    }

    public void setMajorIncidentAfterMins(int majorIncidentAfterMins) {
        this.majorIncidentAfterMins = majorIncidentAfterMins;
    }

    public String getSlmType() {
        return slmType;
    }

    public void setSlmType(String slmType) {
        this.slmType = slmType;
    }

    public String getMonitoringCategory() {
        return monitoringCategory;
    }

    public void setMonitoringCategory(String monitoringCategory) {
        this.monitoringCategory = monitoringCategory;
    }

    public String getMonitoringApplication() {
        return monitoringApplication;
    }

    public void setMonitoringApplication(String monitoringApplication) {
        this.monitoringApplication = monitoringApplication;
    }

    public String getDataConfidentiality() {
        return dataConfidentiality;
    }

    public void setDataConfidentiality(String dataConfidentiality) {
        this.dataConfidentiality = dataConfidentiality;
    }

    public int getLevel3operationUsersId() {
        return level3operationUsersId;
    }

    public void setLevel3operationUsersId(int level3operationUsersId) {
        this.level3operationUsersId = level3operationUsersId;
    }

    public int getInfrastructureContactBpId() {
        return infrastructureContactBpId;
    }

    public void setInfrastructureContactBpId(int infrastructureContactBpId) {
        this.infrastructureContactBpId = infrastructureContactBpId;
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public int getSd2sdCategoryId() {
        return sd2sdCategoryId;
    }

    public void setSd2sdCategoryId(int sd2sdCategoryId) {
        this.sd2sdCategoryId = sd2sdCategoryId;
    }

    public String getSd2sdCapgeminiService() {
        return sd2sdCapgeminiService;
    }

    public void setSd2sdCapgeminiService(String sd2sdCapgeminiService) {
        this.sd2sdCapgeminiService = sd2sdCapgeminiService;
    }
}
