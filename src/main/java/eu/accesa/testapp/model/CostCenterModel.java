package eu.accesa.testapp.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.io.Serializable;
import java.util.List;

@Entity
public class CostCenterModel implements Serializable {
    @Id
    private Long id;
    private String name;
    @OneToMany(fetch = FetchType.EAGER,mappedBy = "ref2US")
    @JsonIgnore
    private List<ServiceCostCenterRelation> serviceCostCenterRelationList;

    public CostCenterModel(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public CostCenterModel() {
        //default constructor
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<ServiceCostCenterRelation> getServiceCostCenterRelationList() {
        return serviceCostCenterRelationList;
    }

    public void setServiceCostCenterRelationList(List<ServiceCostCenterRelation> serviceCostCenterRelationList) {
        this.serviceCostCenterRelationList = serviceCostCenterRelationList;
    }
}
