package eu.accesa.testapp.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.sql.Time;
import java.util.Date;

/**
 * Created by dragos.doicar on 11/1/2016.
 */

@Entity(name="ADMIN.CFG_RELATIONS")
public class CfgRelations {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    @NotNull
    private String tableName1;
    @NotNull
    private String id1;
    @NotNull
    @Column(name="relation_type")
    private String relationType;
    @NotNull
    private String tableName2;
    @NotNull
    private String id2;
    private String value1;
    private String value2;
    private String value3;
    private String remark;
    @Column(name="created_by_users_id")
    private int createdByUsersId;
    @Column(name="created_date")
    private Date createdDate;
    @Column(name="created_time")
    private Time createdTime;
    @Column(name="modified_by_users_id")
    private int modifiedByUsersId;
    @Column(name="modified_date")
    private Date modifiedDate;
    @Column(name="modified_time")
    private Time modifiedTime;
    @NotNull
    @Column(name="is_active")
    private int isActive;

    public CfgRelations(String tableName1, String id1, String relationType, String tableName2, String id2, String value1, String value2, String value3, String remark, int createdByUsersId, Date createdDate, Time createdTime, int modifiedByUsersId, Date modifiedDate, Time modifiedTime, int isActive) {
        this.tableName1 = tableName1;
        this.id1 = id1;
        this.relationType = relationType;
        this.tableName2 = tableName2;
        this.id2 = id2;
        this.value1 = value1;
        this.value2 = value2;
        this.value3 = value3;
        this.remark = remark;
        this.createdByUsersId = createdByUsersId;
        this.createdDate = createdDate;
        this.createdTime = createdTime;
        this.modifiedByUsersId = modifiedByUsersId;
        this.modifiedDate = modifiedDate;
        this.modifiedTime = modifiedTime;
        this.isActive = isActive;
    }

    public CfgRelations() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTableName1() {
        return tableName1;
    }

    public void setTableName1(String tableName1) {
        this.tableName1 = tableName1;
    }

    public String getId1() {
        return id1;
    }

    public void setId1(String id1) {
        this.id1 = id1;
    }

    public String getRelationType() {
        return relationType;
    }

    public void setRelationType(String relationType) {
        this.relationType = relationType;
    }

    public String getTableName2() {
        return tableName2;
    }

    public void setTableName2(String tableName2) {
        this.tableName2 = tableName2;
    }

    public String getId2() {
        return id2;
    }

    public void setId2(String id2) {
        this.id2 = id2;
    }

    public String getValue1() {
        return value1;
    }

    public void setValue1(String value1) {
        this.value1 = value1;
    }

    public String getValue2() {
        return value2;
    }

    public void setValue2(String value2) {
        this.value2 = value2;
    }

    public String getValue3() {
        return value3;
    }

    public void setValue3(String value3) {
        this.value3 = value3;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public int getCreatedByUsersId() {
        return createdByUsersId;
    }

    public void setCreatedByUsersId(int createdByUsersId) {
        this.createdByUsersId = createdByUsersId;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Time getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Time createdTime) {
        this.createdTime = createdTime;
    }

    public int getModifiedByUsersId() {
        return modifiedByUsersId;
    }

    public void setModifiedByUsersId(int modifiedByUsersId) {
        this.modifiedByUsersId = modifiedByUsersId;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public Time getModifiedTime() {
        return modifiedTime;
    }

    public void setModifiedTime(Time modifiedTime) {
        this.modifiedTime = modifiedTime;
    }

    public int getIsActive() {
        return isActive;
    }

    public void setIsActive(int isActive) {
        this.isActive = isActive;
    }
}
