package eu.accesa.testapp.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.sql.Time;
import java.util.Date;

/**
 * Created by dragos.doicar on 11/1/2016.
 */

@Entity(name="ADMIN.SCM_RELATION_APPL_SYSTEM")
public class RelationApplSystem {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private int id;
    @NotNull
    @Column(name="bussines_system_id")
    private int bussinesSystemId;
    @NotNull
    @Column(name="application_id")
    private int applicationId;
    @Column(name="created_by_users_id")
    private int createdByUsersId;
    @Column(name="created_date")
    private Date createdDate;
    @Column(name="created_time")
    private Time createdTime;
    @Column(name="modified_by_users_id")
    private int modifiedByUsersId;
    @Column(name="modified_date")
    private Date modifiedDate;
    @Column(name="modified_time")
    private Time modifiedTime;
    @NotNull
    @Column(name="is_active")
    private int isActive;

    public RelationApplSystem(int bussinesSystemId, int applicationId, int createdByUsersId, Date createdDate, Time createdTime, int modifiedByUsersId, Date modifiedDate, Time modifiedTime, int isActive) {
        this.bussinesSystemId = bussinesSystemId;
        this.applicationId = applicationId;
        this.createdByUsersId = createdByUsersId;
        this.createdDate = createdDate;
        this.createdTime = createdTime;
        this.modifiedByUsersId = modifiedByUsersId;
        this.modifiedDate = modifiedDate;
        this.modifiedTime = modifiedTime;
        this.isActive = isActive;
    }

    public RelationApplSystem() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getBussinesSystemId() {
        return bussinesSystemId;
    }

    public void setBussinesSystemId(int bussinesSystemId) {
        this.bussinesSystemId = bussinesSystemId;
    }

    public int getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(int applicationId) {
        this.applicationId = applicationId;
    }

    public int getCreatedByUsersId() {
        return createdByUsersId;
    }

    public void setCreatedByUsersId(int createdByUsersId) {
        this.createdByUsersId = createdByUsersId;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Time getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Time createdTime) {
        this.createdTime = createdTime;
    }

    public int getModifiedByUsersId() {
        return modifiedByUsersId;
    }

    public void setModifiedByUsersId(int modifiedByUsersId) {
        this.modifiedByUsersId = modifiedByUsersId;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public Time getModifiedTime() {
        return modifiedTime;
    }

    public void setModifiedTime(Time modifiedTime) {
        this.modifiedTime = modifiedTime;
    }

    public int getIsActive() {
        return isActive;
    }

    public void setIsActive(int isActive) {
        this.isActive = isActive;
    }
}
