package eu.accesa.testapp.controller;

import eu.accesa.testapp.model.ServiceInstances;
import eu.accesa.testapp.service.ServiceInstancesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by dragos.doicar on 11/2/2016.
 */
@RestController
public class ServiceInstancesController {
    @Autowired
    private ServiceInstancesService serviceInstancesService;

    @RequestMapping(value="/serviceInstances", method = RequestMethod.GET)
    public ResponseEntity<List<ServiceInstances>> getAll(){
        return new ResponseEntity<List<ServiceInstances>>(serviceInstancesService.findAll(), HttpStatus.OK);
    }

    @RequestMapping(value="/serviceInstances/{id}", method = RequestMethod.GET)
    public ServiceInstances getById(@PathVariable int id){
        return serviceInstancesService.findOne(id);
    }
}
