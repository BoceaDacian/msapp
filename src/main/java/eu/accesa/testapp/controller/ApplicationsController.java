package eu.accesa.testapp.controller;

import eu.accesa.testapp.model.Applications;
import eu.accesa.testapp.service.ApplicationsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by dragos.doicar on 11/2/2016.
 */
@RestController

public class ApplicationsController {
    @Autowired
    private ApplicationsService applicationsService;

    public ApplicationsController(){
        //default constructor
    }

    @RequestMapping(value = "/applications", method = RequestMethod.GET)
    public ResponseEntity<List<Applications>> getAll() {
        return new ResponseEntity<>(applicationsService.findAll(), HttpStatus.OK);
    }

    @RequestMapping(value = "/applications/{id}", method = RequestMethod.GET)
    @ResponseBody
    public Applications getById(@PathVariable int id){
       return applicationsService.findOne(id);
    }
}
