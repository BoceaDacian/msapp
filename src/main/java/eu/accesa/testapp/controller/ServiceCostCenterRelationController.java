package eu.accesa.testapp.controller;

import eu.accesa.testapp.model.ServiceCostCenterRelation;
import eu.accesa.testapp.to.ServiceCostCenterRelationTo;
import eu.accesa.testapp.service.ServiceCostCenterRelationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ServiceCostCenterRelationController {

    @Autowired
    private ServiceCostCenterRelationService serviceCostCenterRelationService;

    @RequestMapping(value = "/costs", method = RequestMethod.POST)
    public ResponseEntity<ServiceCostCenterRelation> addServiceCostRelation(@RequestBody ServiceCostCenterRelationTo appDto) {
        return new ResponseEntity<>(serviceCostCenterRelationService.save(appDto.getAppId(),appDto.getSupportId(),appDto.getCost()), HttpStatus.OK);
    }
}
