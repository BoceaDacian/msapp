package eu.accesa.testapp.controller;

import eu.accesa.testapp.model.RelationApplSystem;
import eu.accesa.testapp.service.RelationApplSystemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by dragos.doicar on 11/2/2016.
 */
@RestController
public class RelationApplSystemController {
    @Autowired
    private RelationApplSystemService relationApplSystemService;

    @RequestMapping(value="/relationApplSystem", method = RequestMethod.GET)
    public ResponseEntity<List<RelationApplSystem>> getAll(){
        return new ResponseEntity<List<RelationApplSystem>>(relationApplSystemService.findAll(), HttpStatus.OK);
    }

    @RequestMapping(value="/relationApplSystem/{id}", method = RequestMethod.GET)
    public RelationApplSystem getById(@PathVariable int id){
        return relationApplSystemService.findOne(id);
    }
}
