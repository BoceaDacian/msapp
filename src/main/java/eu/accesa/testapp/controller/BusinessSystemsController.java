package eu.accesa.testapp.controller;

import eu.accesa.testapp.model.BusinessSystems;
import eu.accesa.testapp.service.BusinessSystemsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by dragos.doicar on 11/2/2016.
 */
@RestController
public class BusinessSystemsController {
    @Autowired
    private BusinessSystemsService businessSystemsService;

    @RequestMapping(value="/businessSystems", method = RequestMethod.GET)
    public ResponseEntity<List<BusinessSystems>> getAll(){
        return new ResponseEntity<>(businessSystemsService.findAll(), HttpStatus.OK);
    }

    @RequestMapping(value="/businessSystems/{id}", method = RequestMethod.GET)
    public BusinessSystems getById(@PathVariable int id){
        return businessSystemsService.findOne(id);
    }
}
