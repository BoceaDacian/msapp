package eu.accesa.testapp.controller;

import eu.accesa.testapp.model.CostCenterModel;
import eu.accesa.testapp.model.ServiceCostCenterRelation;
import eu.accesa.testapp.service.CostCenterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class CostCenterController {
    @Autowired
    private CostCenterService costCenterService;


    public CostCenterController() {
        //default constructor
    }

    @RequestMapping(value = "/support", method = RequestMethod.GET)
    public ResponseEntity<List<CostCenterModel>> getAll() {
        return new ResponseEntity<>(costCenterService.findAll(), HttpStatus.OK);
    }

    @RequestMapping(value = "/supportById", method = RequestMethod.GET)
    public ResponseEntity<List<ServiceCostCenterRelation>> getAllById(@RequestParam Long id){
        CostCenterModel costCenterModel = costCenterService.findOne(id);
        return new ResponseEntity<>(costCenterModel.getServiceCostCenterRelationList(),HttpStatus.OK);
    }
}
