package eu.accesa.testapp.controller;

import eu.accesa.testapp.model.BusinessServices;
import eu.accesa.testapp.service.BusinessServicesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by dragos.doicar on 11/2/2016.
 */
@RestController
public class BusinessServicesController {
    @Autowired
    private BusinessServicesService businessServicesService;

    @RequestMapping(value="/businessServices", method= RequestMethod.GET)
    public ResponseEntity<List<BusinessServices>> getAll(){
        return new ResponseEntity<>(businessServicesService.findAll(), HttpStatus.OK);
    }

    @RequestMapping(value="/businessServices/{id}", method=RequestMethod.GET)
    public BusinessServices getById(@PathVariable int id){
        return businessServicesService.findOne(id);
    }

}
