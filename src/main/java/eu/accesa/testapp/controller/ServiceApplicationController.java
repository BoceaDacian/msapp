package eu.accesa.testapp.controller;

import eu.accesa.testapp.model.ServiceApplicationModel;
import eu.accesa.testapp.repository.ServiceApplicationRepo;
import eu.accesa.testapp.service.ServiceApplicationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ServiceApplicationController {

    @Autowired
    ServiceApplicationService serviceApplicationService;

    @RequestMapping(value = "/services", method = RequestMethod.GET)
    public ResponseEntity<List<ServiceApplicationModel>> getAll() {
        return new ResponseEntity<>(serviceApplicationService.findAll(), HttpStatus.OK);
    }

    @RequestMapping(value = "/services/{period}", method = RequestMethod.GET)
    @ResponseBody
    public List<ServiceApplicationModel> getServiceModelByPeriod(@PathVariable int period){
        return serviceApplicationService.findByPer(period);
    }
}
