package eu.accesa.testapp.controller;

import eu.accesa.testapp.model.CfgRelations;
import eu.accesa.testapp.service.CfgRelationsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by dragos.doicar on 11/2/2016.
 */
@RestController
public class CfgRelationsController {
    @Autowired
    private CfgRelationsService cfgRelationsService;

    @RequestMapping(value="/cfgRelations", method = RequestMethod.GET)
    public ResponseEntity<List<CfgRelations>> getAll(){
        return new ResponseEntity<>(cfgRelationsService.findAll(), HttpStatus.OK);
    }

    @RequestMapping(value="/cfgRelations/{id}", method = RequestMethod.GET)
    public CfgRelations getById(@PathVariable int id){
        return cfgRelationsService.findOne(id);
    }

    @RequestMapping(value="/cfgRelations", method = RequestMethod.PUT)
    public CfgRelations addRelation(CfgRelations cfgRelations){
        return cfgRelationsService.save(cfgRelations);
    }
    @RequestMapping(value="/cfgRelations/{id}", method = RequestMethod.DELETE)
    public void deleteRelation(@PathVariable int id){
        cfgRelationsService.delete(id);
    }
}
