package eu.accesa.testapp.service;

import eu.accesa.testapp.model.Applications;

import java.util.List;

/**
 * Created by dragos.doicar on 11/2/2016.
 */
public interface ApplicationsService {
    List<Applications> findAll();
    Applications findOne(int id);
}
