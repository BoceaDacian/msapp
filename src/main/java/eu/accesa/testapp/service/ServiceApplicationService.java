package eu.accesa.testapp.service;

import eu.accesa.testapp.model.ServiceApplicationModel;
import java.util.List;

public interface ServiceApplicationService {
     List<ServiceApplicationModel> findAll();
     List<ServiceApplicationModel> findByPer(int per);
}
