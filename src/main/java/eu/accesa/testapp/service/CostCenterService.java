package eu.accesa.testapp.service;

import eu.accesa.testapp.model.CostCenterModel;

import java.util.List;

public interface CostCenterService {
    List<CostCenterModel> findAll();
    CostCenterModel findOne(Long id);
}
