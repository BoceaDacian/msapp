package eu.accesa.testapp.service;

import eu.accesa.testapp.model.ServiceApplicationModel;
import eu.accesa.testapp.repository.ServiceApplicationRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class ServiceApplicationServiceImpl implements ServiceApplicationService {

    @Autowired
    private ServiceApplicationRepo serviceApplicationRepo;


    @Override
    public List<ServiceApplicationModel> findAll() {
        return serviceApplicationRepo.findAll();
    }

    @Override
    public List<ServiceApplicationModel> findByPer(int per) {
        return serviceApplicationRepo.findByPer(per);
    }
}
