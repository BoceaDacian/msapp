package eu.accesa.testapp.service;

import eu.accesa.testapp.model.ServiceInstances;

import java.util.List;

/**
 * Created by dragos.doicar on 11/2/2016.
 */

public interface ServiceInstancesService {
    List<ServiceInstances> findAll();
    ServiceInstances findOne(int id);
}
