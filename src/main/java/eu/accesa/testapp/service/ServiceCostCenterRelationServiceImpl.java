package eu.accesa.testapp.service;

import eu.accesa.testapp.model.ServiceApplicationModel;
import eu.accesa.testapp.model.ServiceCostCenterRelation;
import eu.accesa.testapp.repository.ServiceApplicationRepo;
import eu.accesa.testapp.repository.CostCenterRepo;
import eu.accesa.testapp.repository.ServiceCostCenterRelationRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ServiceCostCenterRelationServiceImpl implements ServiceCostCenterRelationService {
    @Autowired
    private CostCenterRepo costCenterRepo;
    @Autowired
    private ServiceApplicationRepo serviceApplicationRepo;
    @Autowired
    private ServiceCostCenterRelationRepo serviceCostCenterRelationRepo;

    @Override
    public ServiceCostCenterRelation save(String appId, Long supportId, Integer cost) {
        ServiceCostCenterRelation app=new ServiceCostCenterRelation();
        app.setCost(cost);
        ServiceApplicationModel appServ=serviceApplicationRepo.findOne(appId);
        app.setRef2Dienst(appServ);
        app.setRef2US(costCenterRepo.findOne(supportId));
        app.setPeriod(appServ.getPer());
        return serviceCostCenterRelationRepo.save(app);
    }
}
