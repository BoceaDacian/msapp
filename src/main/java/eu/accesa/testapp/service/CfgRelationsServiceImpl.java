package eu.accesa.testapp.service;

import eu.accesa.testapp.model.CfgRelations;
import eu.accesa.testapp.repository.CfgRelationsRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by dragos.doicar on 11/2/2016.
 */
@Service
public class CfgRelationsServiceImpl implements CfgRelationsService{

    @Autowired
    private CfgRelationsRepo cfgRelationsRepo;

    @Override
    public List<CfgRelations> findAll() {
        return cfgRelationsRepo.findAll();
    }

    @Override
    public CfgRelations findOne(int id) {
        return cfgRelationsRepo.findOne(id);
    }

    @Override
    public CfgRelations save(CfgRelations cfgRelations) {
        return cfgRelationsRepo.save(cfgRelations);
    }

    @Override
    public void delete(int id) {
        cfgRelationsRepo.findOne(id).setIsActive(0);
    }
}
