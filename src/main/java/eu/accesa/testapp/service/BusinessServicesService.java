package eu.accesa.testapp.service;

import eu.accesa.testapp.model.BusinessServices;

import java.util.List;

/**
 * Created by dragos.doicar on 11/2/2016.
 */
public interface BusinessServicesService {
    List<BusinessServices> findAll();
    BusinessServices findOne(int id);
}
