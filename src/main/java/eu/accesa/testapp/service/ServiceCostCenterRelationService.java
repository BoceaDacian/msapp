package eu.accesa.testapp.service;

import eu.accesa.testapp.model.ServiceCostCenterRelation;

public interface ServiceCostCenterRelationService {

    ServiceCostCenterRelation save(String appId, Long supportId, Integer cost);
}
