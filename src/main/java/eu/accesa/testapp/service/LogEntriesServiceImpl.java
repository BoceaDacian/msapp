package eu.accesa.testapp.service;

import eu.accesa.testapp.model.LogEntries;
import eu.accesa.testapp.repository.LogEntriesRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by dragos.doicar on 11/2/2016.
 */
@Service
public class LogEntriesServiceImpl implements LogEntriesService{

    @Autowired
    private LogEntriesRepo logEntriesRepo;

    @Override
    public LogEntries save(LogEntries logEntries) {
        return logEntriesRepo.save(logEntries);
    }
}
