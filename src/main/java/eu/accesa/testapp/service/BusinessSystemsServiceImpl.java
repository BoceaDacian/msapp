package eu.accesa.testapp.service;

import eu.accesa.testapp.model.BusinessSystems;
import eu.accesa.testapp.repository.BusinessServicesRepo;
import eu.accesa.testapp.repository.BusinessSystemsRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by dragos.doicar on 11/2/2016.
 */
@Service
public class BusinessSystemsServiceImpl implements BusinessSystemsService{
    @Autowired
    private BusinessSystemsRepo businessSystemsRepo;

    @Override
    public List<BusinessSystems> findAll() {
        return businessSystemsRepo.findAll();
    }

    @Override
    public BusinessSystems findOne(int id) {
        return businessSystemsRepo.findOne(id);
    }
}
