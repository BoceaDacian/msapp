package eu.accesa.testapp.service;

import eu.accesa.testapp.model.ServiceInstances;
import eu.accesa.testapp.repository.ServiceInstancesRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by dragos.doicar on 11/2/2016.
 */
@Service
public class ServiceInstancesServiceImpl implements ServiceInstancesService{

    @Autowired
    ServiceInstancesRepo serviceInstancesRepo;

    @Override
    public List<ServiceInstances> findAll() {
        return serviceInstancesRepo.findAll();
    }

    @Override
    public ServiceInstances findOne(int id) {
        return serviceInstancesRepo.findOne(id);
    }
}
