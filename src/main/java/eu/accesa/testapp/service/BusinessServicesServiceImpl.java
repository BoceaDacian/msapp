package eu.accesa.testapp.service;

import eu.accesa.testapp.model.BusinessServices;
import eu.accesa.testapp.repository.BusinessServicesRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by dragos.doicar on 11/2/2016.
 */
@Service
public class BusinessServicesServiceImpl implements BusinessServicesService{

    @Autowired
    BusinessServicesRepo businessServicesRepo;

    @Override
    public List<BusinessServices> findAll() {
        return businessServicesRepo.findAll();
    }

    @Override
    public BusinessServices findOne(int id) {
        return businessServicesRepo.findOne(id);
    }
}
