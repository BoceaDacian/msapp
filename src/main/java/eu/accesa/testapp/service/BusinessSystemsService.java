package eu.accesa.testapp.service;

import eu.accesa.testapp.model.BusinessSystems;

import java.util.List;

/**
 * Created by dragos.doicar on 11/2/2016.
 */
public interface BusinessSystemsService {
    List<BusinessSystems> findAll();
    BusinessSystems findOne(int id);
}
