package eu.accesa.testapp.service;

import eu.accesa.testapp.model.LogEntries;

/**
 * Created by dragos.doicar on 11/2/2016.
 */
public interface LogEntriesService {
    LogEntries save(LogEntries logEntries);
}
