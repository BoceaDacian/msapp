package eu.accesa.testapp.service;

import eu.accesa.testapp.model.RelationApplSystem;

import java.util.List;

/**
 * Created by dragos.doicar on 11/2/2016.
 */
public interface RelationApplSystemService {
    List<RelationApplSystem> findAll();
    RelationApplSystem findOne(int id);
}
