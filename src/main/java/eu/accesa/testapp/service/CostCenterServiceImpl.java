package eu.accesa.testapp.service;

import eu.accesa.testapp.model.CostCenterModel;
import eu.accesa.testapp.repository.CostCenterRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CostCenterServiceImpl implements CostCenterService{
    @Autowired
    private CostCenterRepo costCenterRepo;
    @Override
    public List<CostCenterModel> findAll() {
        return costCenterRepo.findAll();
    }

    @Override
    public CostCenterModel findOne(Long id) {
        return costCenterRepo.findOne(id);
    }
}
