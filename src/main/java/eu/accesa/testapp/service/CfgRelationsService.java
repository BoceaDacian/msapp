package eu.accesa.testapp.service;

import eu.accesa.testapp.model.CfgRelations;

import java.util.List;

/**
 * Created by dragos.doicar on 11/2/2016.
 */
public interface CfgRelationsService {
    List<CfgRelations> findAll();
    CfgRelations findOne(int id);
    CfgRelations save(CfgRelations cfgRelations);
    void delete(int id);


}
