package eu.accesa.testapp.service;

import eu.accesa.testapp.model.RelationApplSystem;
import eu.accesa.testapp.repository.RelationApplSystemRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by dragos.doicar on 11/2/2016.
 */
@Service
public class RelationApplSystemServiceImpl implements RelationApplSystemService{

    @Autowired
    private RelationApplSystemRepo relationApplSystemRepo;

    @Override
    public List<RelationApplSystem> findAll() {
        return relationApplSystemRepo.findAll();
    }

    @Override
    public RelationApplSystem findOne(int id) {
        return relationApplSystemRepo.findOne(id);
    }
}
