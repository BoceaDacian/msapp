package eu.accesa.testapp.service;

import eu.accesa.testapp.model.Applications;
import eu.accesa.testapp.repository.ApplicationsRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by dragos.doicar on 11/2/2016.
 */
@Service
public class ApplicationsServiceImpl implements ApplicationsService{
    @Autowired
    ApplicationsRepo applicationsRepo;

    @Override
    public List<Applications> findAll() {
        return applicationsRepo.findAll();
    }

    @Override
    public Applications findOne(int id) {
        return applicationsRepo.findOne(id);
    }
}
